package ru.virarnd.zadolbaliapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import ru.virarnd.zadolbaliapp.rest.DatabaseHelper;
import ru.virarnd.zadolbaliapp.rest.MyTestService;
import ru.virarnd.zadolbaliapp.rest.ZadolbReceiver;

public class MainActivity extends AppCompatActivity {

    final String LOG_TAG = "myLogs";


    private Toolbar mToolbar;
    private View container;
    private DrawerLayout drawerLayout;

    DatabaseHelper databaseHelper;

    ZadolbReceiver zadolbReceiver = new ZadolbReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(LOG_TAG, "onCreate");

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.accent));

        setupDrawerLayout();

        container = findViewById(R.id.fragment_frame);

        startService(new Intent(this, MyTestService.class));

        databaseHelper = new DatabaseHelper(this);

        registerBroadcastReceiver(container);


    }


    public void registerBroadcastReceiver(View view) {
        this.registerReceiver(zadolbReceiver, new IntentFilter(
                "android.intent.action.TIME_TICK"));
        Toast.makeText(getApplicationContext(), "Приёмник включен",
                Toast.LENGTH_SHORT).show();
    }

    // Отменяем регистрацию
    public void unregisterBroadcastReceiver(View view) {
        this.unregisterReceiver(zadolbReceiver);
        Toast.makeText(getApplicationContext(), "Приёмник выключён", Toast.LENGTH_SHORT)
                .show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (item.getItemId() == R.id.action_example) {
            Snackbar.make(container, " pressed", Snackbar.LENGTH_SHORT).show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    private void setupDrawerLayout() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView view = (NavigationView) findViewById(R.id.navigation_view);
        view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                drawerNavigation(menuItem);
                menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                Snackbar.make(container, menuItem.getTitle() + " pressed", Snackbar.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    private void drawerNavigation(MenuItem menuItem) {
        Fragment myFragment;

        switch (menuItem.getItemId()) {
            case R.id.nav_main_screen:
                myFragment = new FragmentZadolbali();
                break;
            case R.id.nav_favorites:
                myFragment = new FragmentFavorites();
                break;
            case R.id.nav_about:
                myFragment = new FragmentAbout();
                break;
            default:
                myFragment = new FragmentZadolbali();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, myFragment).addToBackStack(null).commit();
    }


}
