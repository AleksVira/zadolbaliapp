package ru.virarnd.zadolbaliapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class FragmentZadolbali extends Fragment {
    private CardAdapter cardAdapter;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View zadolbaliView = inflater.inflate(R.layout.fragment_zadolbali, container, false);




        recyclerView = (RecyclerView) zadolbaliView.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);

        cardAdapter = new CardAdapter(createItemList());
        recyclerView.setAdapter(cardAdapter);

        return zadolbaliView;
    }


    // Вспомогательный метод для подготовки списка
    public List<Quote> createItemList() {
        List<Quote> itemList = new ArrayList<Quote>();
        for (int i = 0; i < 40; i++) {
            itemList.add(new Quote("Заголовок "+i, "Текст " + i, "Дата "+i, false));
        }

        Quote change1 = new Quote("Заголовок ****", "Послушайте, я не социопат, не модный здесь интроверт, я люблю людей, в том числе своих соседей по квартире. Но меня задолбали.\n" +
                "\n" +
                "Если вы снимаете квартиру на двоих-троих, как-то подразумевается, что надо уважать образ жизни друг друга. Мы живём каждый в своей комнате, друг к другу не лезем, но есть ведь и общие места: кухня, ванная, коридор. А ещё общий холодильник. И вы, дорогие соседи, задолбали таскать продукты.\n" +
                "\n" +
                "Утром просыпаюсь, собираюсь на работу, времени мало, завтрак продумал ещё с вечера, открываю холодильник: яиц нет, от колбасы остался только огрызок, в хлебнице пусто. Вы это всё покупали? Нет. Вы голодающие безденежные и безработные? Нет. Хорошо, спросить можно было? Нет, вы сочли это лишним. Результат: я убегаю на работу голодным, вы делаете удивлённое лицо «ну чё ты завёлся из-за пары яиц и куска хлеба», а потом вечером я иду в магазин и приволакиваю сумку продуктов, а вы сидите за компом.", "Дата 00000", false);

        itemList.set(1, change1);


        return itemList;

    }



}
