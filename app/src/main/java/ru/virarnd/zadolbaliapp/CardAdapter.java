package ru.virarnd.zadolbaliapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardViewHolder> {
    private List<Quote> quoteList;

    public CardAdapter(List<Quote> quoteList) {
        this.quoteList = quoteList;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.recycler_zadolb_item, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CardViewHolder viewHolder, int position) {
        Quote holder = quoteList.get(position);
        viewHolder.nTitle.setText(holder.title);
        viewHolder.nQuote.setText(holder.quoteText);
        viewHolder.nDate.setText(holder.date);

        //TODO Сделать обработку кнокпи "Избранное"
//        viewHolder.nFavorites.bringToFront();
    }

    @Override
    public int getItemCount() {
        return quoteList == null ? 0 : quoteList.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        protected TextView nTitle;
        protected TextView nQuote;
        protected TextView nDate;
        protected ImageButton nFavorites;

        public CardViewHolder(View itemView) {
            super(itemView);
            nTitle = (TextView) itemView.findViewById(R.id.quote_title);
            nQuote = (TextView) itemView.findViewById(R.id.quote_text);
            nDate = (TextView) itemView.findViewById(R.id.quote_date);
            nFavorites = (ImageButton) itemView.findViewById(R.id.quote_favorite);


        }
    }
}


