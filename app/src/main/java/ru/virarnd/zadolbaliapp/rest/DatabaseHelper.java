package ru.virarnd.zadolbaliapp.rest;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;
import java.util.Objects;

import ru.virarnd.zadolbaliapp.rest.model.UserModel;

public class DatabaseHelper extends SQLiteOpenHelper implements BaseColumns {
    final String LOG_TAG = "myLogs";

    public static final String DATABASE_TABLE = "quotes";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "zadolbaliDatabase.db";

    public static final String DESCRIPTION_COLUMN = "desc";
    public static final String ELEMENT_COLUMN = "element";
    public static final String LINK_COLUMN = "link";
    public static final String NAME_COLUMN = "name";
    public static final String SITE_COLUMN = "site";

    private static final String DATABASE_CREATE_SCRIPT = "create table "
            + DATABASE_TABLE + " (" + BaseColumns._ID + " integer primary key autoincrement, "
            + DESCRIPTION_COLUMN + " text not null, "
            + ELEMENT_COLUMN + " text not null, "
            + LINK_COLUMN + " text not null, "
            + NAME_COLUMN + " text not null, "
            + SITE_COLUMN + " text not null);";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Запишем в журнал
        Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);
        // Удаляем старую таблицу и создаём новую
        db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE);
        // Создаём новую таблицу
        onCreate(db);
    }



}

