package ru.virarnd.zadolbaliapp.rest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SQLController {

    private DatabaseHelper dbHelper;
    private Context ourcontext;
    private SQLiteDatabase database;

    public SQLController(Context c) {
        ourcontext = c;
    }

    public SQLController open() throws SQLException {
        dbHelper = new DatabaseHelper(ourcontext);
        database = dbHelper.getWritableDatabase();
        return this;

    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String desc, String element, String link, String name, String site) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper.DESCRIPTION_COLUMN, desc);
        contentValue.put(DatabaseHelper.ELEMENT_COLUMN, element);
        contentValue.put(DatabaseHelper.LINK_COLUMN, link);
        contentValue.put(DatabaseHelper.NAME_COLUMN, name);
        contentValue.put(DatabaseHelper.SITE_COLUMN, site);
        database.insert(DatabaseHelper.DATABASE_TABLE, null, contentValue);

    }

    public Cursor fetch() {
        String[] columns = new String[]{DatabaseHelper._ID,
                DatabaseHelper.DESCRIPTION_COLUMN,
                DatabaseHelper.ELEMENT_COLUMN,
                DatabaseHelper.LINK_COLUMN,
                DatabaseHelper.NAME_COLUMN,
                DatabaseHelper.SITE_COLUMN};
        Cursor cursor = database.query(DatabaseHelper.DATABASE_TABLE, columns, null,
                null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public int update(long _id, String name, String desc) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.DESCRIPTION_COLUMN, name);
        contentValues.put(DatabaseHelper.ELEMENT_COLUMN, desc);
        contentValues.put(DatabaseHelper.LINK_COLUMN, desc);
        contentValues.put(DatabaseHelper.NAME_COLUMN, desc);
        contentValues.put(DatabaseHelper.SITE_COLUMN, desc);
        int i = database.update(DatabaseHelper.DATABASE_TABLE, contentValues,
                DatabaseHelper._ID + " = " + _id, null);
        return i;
    }

    public void delete(long _id) {
        database.delete(DatabaseHelper.DATABASE_TABLE, DatabaseHelper._ID + "=" + _id, null);
    }
}