package ru.virarnd.zadolbaliapp.rest;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ru.virarnd.zadolbaliapp.NetworkStatusChecker;
import ru.virarnd.zadolbaliapp.rest.api.Zapros1;
import ru.virarnd.zadolbaliapp.rest.model.UserModel;

public class MyTestService extends Service {

//    public static ArrayList<UserModel> lResponse = new ArrayList<UserModel>();


    private Callback<ArrayList<UserModel>> myResponse = new Callback<ArrayList<UserModel>>() {
        @Override
        public void success(ArrayList<UserModel> s, Response response) {
//            lResponse = (ArrayList<UserModel>) s.clone();
            Log.d(LOG_TAG, "success");
            Log.d(LOG_TAG, s.get(1).getElementPureHtml());
            //TODO Понять как передавать данные наружу
            // В этом месте данные вижу, снаружи уже не могу подступиться
        }

        @Override
        public void failure(RetrofitError error) {
            Log.d(LOG_TAG, "Failed to make http request for: " + error.getUrl());
            Response errorResponse = error.getResponse();
            if (errorResponse != null) {
                Log.d(LOG_TAG, errorResponse.getReason());
                if (errorResponse.getStatus() == 500) {
                    Log.d(LOG_TAG, "Handle Server Errors Here");
                }
            }
        }

        public void getReposAsync(Callback<ArrayList<UserModel>> result) {

        }
    };

    public MyTestService() {
        super();
    }

    public static final String QUOTES_RECEIVED = "ru.virarnd.zadolbaliapp.rest.QUOTES";
    public static final String QUOTES_MESSAGE = "!!!!";


    final String LOG_TAG = "myLogs";
//    private SQLController dbcon;


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public MyTestService(String name) {
//        super(name);
    }

    public void onCreate() {
        super.onCreate();

        Log.d(LOG_TAG, "onCreate");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        sendZapros();
        sendMessage(this);
        return Service.START_STICKY;
    }


    public void sendMessage(Context context) {
        Intent intent = new Intent();
        intent.setAction(QUOTES_RECEIVED);
        intent.putExtra("ru.virarnd.zadolbaliapp.rest.QUOTES", QUOTES_MESSAGE);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }


    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    public void sendZapros() {
        if (NetworkStatusChecker.isNetworkAvailable(this)) {
            Log.d(LOG_TAG, "onRequest");
            final String BASE_URL = "http://www.umori.li/api/";
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint(BASE_URL)
                    .build();

            Zapros1 zapros1 = restAdapter.create(Zapros1.class);
//            Log.d(LOG_TAG, "try to get response");
            zapros1.sendRequestToAPI("zadolba.li", "zadolbali", 40, myResponse);

            Log.d(LOG_TAG, "Request done");
        } else {
            Log.d(LOG_TAG, "Ошибка, нет сети!!!!");
        }

    }

//    public void writeDB() {
//        Log.d(LOG_TAG, "Trying to write");
//        dbcon = new SQLController(this);
//        dbcon.open();
//        for (int i = 0; i < myResponse; i++) {
//
//        }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
