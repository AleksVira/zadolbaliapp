package ru.virarnd.zadolbaliapp.rest;

import android.util.Log;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ru.virarnd.zadolbaliapp.rest.model.UserModel;

class RetroftCallback<S> implements Callback<S> {

    public ArrayList<UserModel> lResponse = new ArrayList<UserModel>();


    final String LOG_TAG = "myLogs";


    @Override
    public void success(S s, Response response) {
        lResponse = (ArrayList<UserModel>) s;
        Log.d(LOG_TAG, "success");
    }

    @Override
    public void failure(RetrofitError error) {

        Log.d(LOG_TAG, "Failed to make http request for: " + error.getUrl());
        Response errorResponse = error.getResponse();
        if (errorResponse != null) {
            Log.d(LOG_TAG, errorResponse.getReason());
            if (errorResponse.getStatus() == 500) {
                Log.d(LOG_TAG, "Handle Server Errors Here");
            }
        }
    }

    public void getReposAsync(Callback<ArrayList<UserModel>> result) {

    }


}