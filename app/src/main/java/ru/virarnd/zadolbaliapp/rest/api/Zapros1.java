package ru.virarnd.zadolbaliapp.rest.api;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import ru.virarnd.zadolbaliapp.rest.model.UserModel;

public interface Zapros1 {

    @GET("/get")
    void sendRequestToAPI(@Query("site") String site,
                          @Query("name") String name,
                          @Query("num") int num,
                          Callback<ArrayList<UserModel>> cb);

}
