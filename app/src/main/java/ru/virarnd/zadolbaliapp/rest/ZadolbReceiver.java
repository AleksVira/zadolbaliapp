package ru.virarnd.zadolbaliapp.rest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ZadolbReceiver extends BroadcastReceiver {
    public ZadolbReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Обнаружено сообщение: " +
                        intent.getStringExtra("ru.virarnd.zadolbaliapp.rest.QUOTES"),
                Toast.LENGTH_LONG).show();
    }
}
