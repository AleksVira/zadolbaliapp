package ru.virarnd.zadolbaliapp;


public class Quote {
    public String title;
    public String quoteText;
    public String date;
    boolean favorite;

    public Quote(String title, String quoteText, String date, boolean favorite) {
        this.title = title;
        this.quoteText = quoteText;
        this.date = date;
        this.favorite = favorite;
    }
}
